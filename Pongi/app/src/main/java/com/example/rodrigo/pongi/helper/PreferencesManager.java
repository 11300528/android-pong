package com.example.rodrigo.pongi.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Juan.
 */
public class PreferencesManager {

    private static String TAG = PreferencesManager.class.getSimpleName();

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context mContext;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "userPref";

    private static final String KEY_IS_USERSET = "isUserSet";
    private static final String KEY_USERNAME = "userName";

    public PreferencesManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setUser(boolean isUserSet, String userName) {
        editor.putBoolean(KEY_IS_USERSET, isUserSet);
        editor.putString(KEY_USERNAME, userName);
        editor.commit();
        Log.d(TAG, "User set");
    }

    public void deleteUser() {
        editor.putBoolean(KEY_IS_USERSET, false);
        editor.putString(KEY_USERNAME, "");
        editor.commit();
        Log.d(TAG, "User deleted");
    }

    public boolean isUserSet() {
        return pref.getBoolean(KEY_IS_USERSET, false);
    }

    public String getUser() {
        return pref.getString(KEY_USERNAME, "");
    }
}
